import argparse
from pathlib import Path
import json

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import PointCloud2, PointField, CompressedImage
from std_msgs.msg import Header
from sensor_msgs_py.point_cloud2 import create_cloud
from cv_bridge import CvBridge

from detect import YoloV7SpatialDetector


class DetectNode(Node):
    def __init__(self, config_path: Path, blob_path: Path, name="yolov7_detect_node"):
        super().__init__(name)
        self.detect_pub = self.create_publisher(PointCloud2, "yolov7_detections", 10)
        self.preview_pub = self.create_publisher(
            CompressedImage, "yolov7_detections_preview", 10
        )

        with config_path.open() as config_file:
            config = json.load(config_file)

        self.detector = YoloV7SpatialDetector(
            classes=config["classes"],
            labels=config["labels"],
            anchors=config["anchors"],
            anchor_masks=config["anchor_masks"],
            iou_threshold=config["iou_threshold"],
            confidence_threshold=config["confidence_threshold"],
            input_size=tuple(config["input_size"].split("x")),
            blob_path=blob_path,
        )

        self.pointcloud_fields = [
            PointField("x", 0, PointField.FLOAT32, 1),
            PointField("y", 0, PointField.FLOAT32, 1),
            PointField("z", 0, PointField.FLOAT32, 1),
        ]
        self.pointcloud_header = Header(frame_id="camera")  # TODO parameterize

        self.cvbr = CvBridge()

    def loop():
        detections = detector.getDetections()
        points = [
            [d.spatialCoordinates.x, d.spatialCoordinates.y, d.spatialCoordinates.z]
            for d in detections
        ]

        self.detect_pub.publish(
            create_cloud(self.pointcloud_header, self.pointcloud_fields, points)
        )

        mat = detector.getPreviewMat(detections)
        self.preview_pub.publish(self.cvbr.cv2_to_compressed_imgmsg(mat))


def run():
    parser = argparse.ArgumentParser(description="Start YoloV7 DepthAI Detection Node.")

    parser.add_argument(
        "--config-file",
        type=Path,
        required=True,
        dest="config_path",
        help="Path to JSON config",
    )
    parser.add_argument(
        "--blob-file",
        type=Path,
        required=True,
        dest="blob_path",
        help="Path to MyriadX blob",
    )

    args = parser.parse_args()

    detect_node = DetectNode(args.config_path, args.blob_path)

    while rclpy.ok():
        rclpy.spin_once(detect_node)
        detect_node.loop()


if __name__ == "__main__":
    run()
