from typing import cast
from dataclasses import dataclass
from pathlib import Path
import time
import depthai as dai
import cv2


@dataclass
class Coords:
    x: int
    y: int
    z: int


# these are kinda weird since I'm actually just defining the type for a depthai data structure
@dataclass
class YoloV7SpatialDetection:
    xmin: float
    ymin: float
    xmax: float
    ymax: float
    confidence: float
    spatialCoordinates: Coords
    label: int  # class index


class YoloV7SpatialDetector:
    def __init__(
        self,
        classes: int,
        labels: list[int],
        coordinates: int,
        anchors: list[float],
        anchor_masks: dict[str, list[int]],
        iou_threshold: float,
        confidence_threshold: float,
        input_size: tuple[int, int],
        blob_path: Path,
    ) -> None:
        # Create pipeline
        pipeline = dai.Pipeline()

        if not blob_path.exists():
            raise FileNotFoundError(f"{str(blob_path)} not found!")

        # Define sources and outputs
        camRgb = pipeline.create(dai.node.ColorCamera)
        spatialDetectionNetwork = cast(
            dai.node.YoloSpatialDetectionNetwork,
            pipeline.create(dai.node.YoloSpatialDetectionNetwork),
        )
        monoLeft = pipeline.create(dai.node.MonoCamera)
        monoRight = pipeline.create(dai.node.MonoCamera)
        stereo = pipeline.create(dai.node.StereoDepth)
        nnNetworkOut = pipeline.create(dai.node.XLinkOut)

        xoutRgb = pipeline.create(dai.node.XLinkOut)
        xoutNN = pipeline.create(dai.node.XLinkOut)
        xoutBoundingBoxDepthMapping = pipeline.create(dai.node.XLinkOut)
        xoutDepth = pipeline.create(dai.node.XLinkOut)

        xoutRgb.setStreamName("rgb")
        xoutNN.setStreamName("detections")
        xoutBoundingBoxDepthMapping.setStreamName("boundingBoxDepthMapping")
        xoutDepth.setStreamName("depth")
        nnNetworkOut.setStreamName("nnNetwork")

        # Properties
        camRgb.setPreviewSize(input_size)
        camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
        camRgb.setInterleaved(False)
        camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

        monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
        monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
        monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
        monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

        # setting node configs
        stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_ACCURACY)
        # Align depth map to the perspective of RGB camera, on which inference is done
        stereo.setDepthAlign(dai.CameraBoardSocket.RGB)
        stereo.setOutputSize(
            monoLeft.getResolutionWidth(), monoLeft.getResolutionHeight()
        )

        spatialDetectionNetwork.setBlobPath(blob_path)
        spatialDetectionNetwork.setConfidenceThreshold(confidence_threshold)
        spatialDetectionNetwork.input.setBlocking(False)
        spatialDetectionNetwork.setBoundingBoxScaleFactor(0.5)
        spatialDetectionNetwork.setDepthLowerThreshold(100)
        spatialDetectionNetwork.setDepthUpperThreshold(11000)
        spatialDetectionNetwork.setSpatialCalculationAlgorithm(
            dai.SpatialLocationCalculatorAlgorithm(1)
        )

        # Yolo specific parameters
        spatialDetectionNetwork.setNumClasses(classes)
        spatialDetectionNetwork.setCoordinateSize(coordinates)
        spatialDetectionNetwork.setAnchors(anchors)
        spatialDetectionNetwork.setAnchorMasks(anchor_masks)
        spatialDetectionNetwork.setIouThreshold(iou_threshold)
        spatialDetectionNetwork.setNumInferenceThreads(2)

        # Linking
        monoLeft.out.link(stereo.left)
        monoRight.out.link(stereo.right)

        camRgb.preview.link(spatialDetectionNetwork.input)
        spatialDetectionNetwork.passthrough.link(xoutRgb.input)

        spatialDetectionNetwork.out.link(xoutNN.input)
        spatialDetectionNetwork.boundingBoxMapping.link(
            xoutBoundingBoxDepthMapping.input
        )

        stereo.depth.link(spatialDetectionNetwork.inputDepth)
        spatialDetectionNetwork.passthroughDepth.link(xoutDepth.input)
        spatialDetectionNetwork.outNetwork.link(nnNetworkOut.input)

        self.device = dai.Device(pipeline)
        self.previewQueue = self.device.getOutputQueue(
            name="rgb", maxSize=4, blocking=False
        )
        self.detectionNNQueue = self.device.getOutputQueue(
            name="detections", maxSize=4, blocking=False
        )
        self.xoutBoundingBoxDepthMappingQueue = self.device.getOutputQueue(
            name="boundingBoxDepthMapping", maxSize=4, blocking=False
        )
        self.depthQueue = self.device.getOutputQueue(
            name="depth", maxSize=4, blocking=False
        )
        self.networkQueue = self.device.getOutputQueue(
            name="nnNetwork", maxSize=4, blocking=False
        )

        self.labels = labels
        self.bbox_color = (255, 255, 255)

        self.last_frame_ns = time.monotonic_ns()  # used for fps
        self.fps = 0

    # should be called in a loop
    def getDetections(self) -> list[YoloV7SpatialDetection]:
        inDet = self.detectionNNQueue.get()
        current_ns = time.monotonic_ns()
        self.fps = 1e9 / (current_ns - self.last_frame_ns)
        return inDet.detections  # type: ignore

    # should be called in a loop
    # Returns an OpenCV mat
    # cv2 doesn't have type information, so we can't specify return type
    def getPreviewMat(self, detections: list[YoloV7SpatialDetection]):
        inPreview = self.previewQueue.get()
        frame = inPreview.getCvFrame()  # type: ignore
        height = frame.shape[0]
        width = frame.shape[1]

        for detection in detections:
            # Denormalize bounding box
            x1 = int(detection.xmin * width)
            x2 = int(detection.xmax * width)
            y1 = int(detection.ymin * height)
            y2 = int(detection.ymax * height)
            label = self.labels[detection.label]
            cv2.putText(
                frame,
                str(label),
                (x1 + 10, y1 + 20),
                cv2.FONT_HERSHEY_TRIPLEX,
                0.5,
                255,
            )
            cv2.putText(
                frame,
                "{:.2f}".format(detection.confidence * 100),
                (x1 + 10, y1 + 35),
                cv2.FONT_HERSHEY_TRIPLEX,
                0.5,
                255,
            )
            cv2.putText(
                frame,
                f"X: {int(detection.spatialCoordinates.x)} mm",
                (x1 + 10, y1 + 50),
                cv2.FONT_HERSHEY_TRIPLEX,
                0.5,
                255,
            )
            cv2.putText(
                frame,
                f"Y: {int(detection.spatialCoordinates.y)} mm",
                (x1 + 10, y1 + 65),
                cv2.FONT_HERSHEY_TRIPLEX,
                0.5,
                255,
            )
            cv2.putText(
                frame,
                f"Z: {int(detection.spatialCoordinates.z)} mm",
                (x1 + 10, y1 + 80),
                cv2.FONT_HERSHEY_TRIPLEX,
                0.5,
                255,
            )
            cv2.rectangle(
                frame, (x1, y1), (x2, y2), self.bbox_color, cv2.FONT_HERSHEY_SIMPLEX
            )

        cv2.putText(
            frame,
            "NN fps: {:.2f}".format(self.fps),
            (2, frame.shape[0] - 4),
            cv2.FONT_HERSHEY_TRIPLEX,
            0.4,
            self.bbox_color,
        )

        return frame
